use std::env;
use std::fs;
use std::fs::File;
use std::path::Path;
use std::io::Read;

static DEFAULT_PATH: &str = ".shortcuts_list";

#[derive(Debug)]
enum Command { Unknown, Find, Add }

fn find_shortcut(data: &str, p: &str, k: &str, s: &str) {
    let arr = data.split('\n');
    let lines: Vec<&str> = arr.collect();
    for line in &lines {
        if p.is_empty() || line.contains(&p) {
            if k.is_empty() || line.contains(&k) {
                if s.is_empty() || line.contains(&s) {
                    println!("{}", line);
                }
            }
        }
    }
}

fn add_shortcut(data: &str, p: &str, k: &str, s: &str) {
    if p.is_empty() || k.is_empty() || s.is_empty() { return; }

    let arr = data.split('\n');
    let lines: Vec<&str> = arr.collect();
    for line in &lines {
        if line.contains(&p) && line.contains(&k) && line.contains(&s) { return; }
    }
    let shortcut = format!("{}{},{},{}\n", data, p, k, s);
    println!("Adding {},{},{}", p, k, s);
    fs::write(DEFAULT_PATH, shortcut).expect("Error writing to file");
}

fn get_data() -> String {
    let path = Path::new(DEFAULT_PATH);
    if path.exists() == false {
        File::create(DEFAULT_PATH).expect("Error creating file");
        println!("Creating file: {}", DEFAULT_PATH);
    }
    let mut file = File::open(&path).expect("Error opening file");
    let mut data = String::new();
    file.read_to_string(&mut data).expect("Error reading file");
    return data;
}

fn usage() {
    println!("Invalid command\nUsage: shortcuts <COMMAND> <OPTIONS>");
    println!("COMMAND\n\t-f\t\t(find)\n\t-a\t\t(add)");
    println!("OPTIONS\n\t-p <program>\n\t-k <keys>\t(e.g. ctrl+tab)\n\t-s <summary>");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() <= 1 || args.len() > 8 {
        usage()
    } else {
        let mut count = 1;
        let mut is_valid = true;
        let mut cmd = Command::Unknown;
        let mut p = "";
        let mut k = "";
        let mut s = "";

        loop {
            if count == args.len() { break; }
            let arg = &args[count];

            match &arg[..] {
                "-f" => { cmd = Command::Find; }
                "-a" => { cmd = Command::Add; }
                "-p" => { p = &args[count+1]; count += 1; }
                "-k" => { k = &args[count+1]; count += 1; }
                "-s" => { s = &args[count+1]; count += 1; }
                _ => { usage(); is_valid = false; break; }
            }
            count += 1;
        }
        if is_valid {
            let data = get_data();
            match cmd {
                Command::Find => { find_shortcut(&data.to_string(), p, k, s); }
                Command::Add => { add_shortcut(&data.to_string(), p, k, s); }
                _ => {}
            }
        }
    }
}
